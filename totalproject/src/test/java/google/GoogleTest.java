package google;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

import amazon.AmazonTest;
import extentreportconfig.ExtentReport;
import extentreportconfig.TakeShot;

public class GoogleTest extends ExtentReport{
	
	  private static final Logger LOGGER = LogManager.getLogger(GoogleTest.class.getName());
	
	public WebDriver driver; 
	String appURL = "http://google.com";
	//  private static final Logger LOGGER1 = LogManager.getLogger(GoogleTest.class.getName());
	
	@BeforeMethod
	public void testSetUp() {
		
	System.setProperty("webdriver.chrome.driver", "C:\\Users\\Admin\\chromedriver.exe");
		  driver = new ChromeDriver();
			driver.navigate().to(appURL);
			driver.manage().window().maximize();
		  
		  System.out.println("tersttest");
	}
	
	@Test
	public void verifyGooglePageTitle1() {
		driver.navigate().to(appURL);
		driver.manage().window().maximize();
		String getTitle = driver.getTitle();
		Assert.assertEquals(getTitle, "Googlee");
	    {
	        LOGGER.debug("Debug Message Logged !!!");
	        LOGGER.info("Info Message Logged !!!");
	       // LOGGER.error("Error Message Logged !!!", new NullPointerException("NullError"));
	    }
	    
	}
	
	@Test
	public void verifyGooglePageTitle2() {
		driver.navigate().to(appURL);
		driver.manage().window().maximize();
		String getTitle = driver.getTitle();
		Assert.assertEquals(getTitle, "Googlle");
	    {
	        LOGGER.debug("Debug Message Logged !!!");
	        LOGGER.info("Info Message Logged !!!");
	       // LOGGER.error("Error Message Logged !!!", new NullPointerException("NullError"));
	    }
	    
	}
	
	@Test
	public void verifyGooglePageTitle3() {
		driver.navigate().to(appURL);
		driver.manage().window().maximize();
		String getTitle = driver.getTitle();
		Assert.assertEquals(getTitle, "Google");
	    {
	        LOGGER.debug("Debug Message Logged !!!");
	        LOGGER.info("Info Message Logged !!!");
	       // LOGGER.error("Error Message Logged !!!", new NullPointerException("NullError"));
	    }
	    
	}
	
	  //  {
	  //      LOGGER.debug("Debug Message Loggedgggggg !!!");
	   //     LOGGER.info("Info Message Loggedgggggg !!!");
	 //   }
@AfterMethod(alwaysRun=true)
@Parameters({"author", "group"})
public void getResult(ITestResult result, String author, String group) throws IOException
{		   // {
		        LOGGER.debug("Debug Message Logged !!!");
		        LOGGER.info("Info Message Logged !!!");
		       // LOGGER.error("Error Message Logged !!!", new NullPointerException("NullError"));
		  //  }
		        
	String methodName = result.getMethod().getMethodName();

	String methodCut = methodName.substring(10);

    if (result.getStatus() == ITestResult.FAILURE)
    {
    	test = extent.createTest(methodName);
    	test.assignCategory("Google. " +group);
    	test.assignAuthor(author);
    	test.info("verify google "  +methodCut+  " process works correctly");
    	test.info("UserID: AutomationUser1");
        String screenShotPath = TakeShot.capture(driver, "screenShotName");
        test.log(Status.FAIL, MarkupHelper.createLabel(methodName+" test case FAILED due to below issues:", ExtentColor.RED));
        test.fail(result.getThrowable());
        test.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));
    }
   else if(result.getStatus() == ITestResult.SUCCESS)
    {
	   test = extent.createTest(methodName);
	test.assignCategory("Google. " +group);
	test.assignAuthor(author);
	test.info("verify google "  +methodCut+  " process works correctly");
	test.info("UserID: AutomationUser1");
        test.log(Status.PASS, MarkupHelper.createLabel(methodName+" test case PASSED", ExtentColor.GREEN));
    }
    else
    {
    	test = extent.createTest(methodName);
    	test.assignCategory("Google. " +group);
    	test.assignAuthor(author);
    	test.info("verify google "  +methodCut+  " process works correctly");
    	test.info("UserID: AutomationUser1");
        test.log(Status.SKIP, MarkupHelper.createLabel(methodName+" test case SKIPPED", ExtentColor.ORANGE));
        test.skip(result.getThrowable());
    }
    
    extent.flush();

    driver.quit();

}

}